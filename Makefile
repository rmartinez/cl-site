all: build

static:
	mkdir -p output/static

build: static
	sbcl --load build.lisp --non-interactive

clean:
	rm -rf output
	mkdir output

serve: static
	sbcl --load test-serve.lisp

paserve: static 
	sbcl --load servers/paserve/serve.lisp

zaserve: static 
	sbcl --load servers/zaserve/serve.lisp

htoot: static 
	sbcl --load servers/htoot/serve.lisp

woo: static 
	sbcl --load servers/woo/serve.lisp

clack: static 
	sbcl --load servers/clack/serve.lisp

cl-http: static 
	sbcl --load servers/cl-http/serve.lisp

