(in-package :cl-site-cl-http)

(defparameter *listeners* 5) 

(defun print-unimplemented-message ()
  (format t "

**********************************

Thank you for trying the clnet site cl-Http test server!

cl-http itself is not available in Quicklisp so it is not loaded
yet. So, the first step would be to get it added to Quicklisp (Reinier
Joswig might be able to help with this; when it's fit for inclusion,
you can make a request at

 https://github.com/quicklisp/quicklisp-projects/issues

Next, the specific code for serving the cl-site pages for cl-http has
not been filled in yet.

If you'd like to volunteer to do this, and maintain it, please fill in
the `publish-cl-site,' `client-test,' and `start-server' functions in
the file cl-site/servers/cl-http/publish.lisp.

Any changes/additions for cl-http support should be contained in
this directory (cl-site/servers/cl-http/), except for the definition and
exporting of a global variable specifying a distinct cl-http
default starting port `cl-site-cl-http:*port*,' which can go in
cl-site/globals.lisp and cl-site/package.lisp respectively.


For implementing the functions below, it may help to follow the
examples in cl-site/servers/paserve/publish.lisp.

**********************************

"))


(defun publish-cl-site (&key (output-dir cl-site:*OUTPUT-DIR*))
  "This function should publish all the site content from the output-dir, both with and without their .html suffices"
  (declare (ignore ouput-dir)))


(defun client-test (port)
  "This function should check whether the given port is free for listening, and return it if so, nil if not."
  (declare (ignore port)))


(defun start-server (&key (port ;;cl-site:*cl-http-port* ;; FLAG Define this global and uncomment this when activated.
			   ) (listeners *listeners*))
  "This function should check for the next available port, starting with the given port, and start a webserver
on that port. If starting on that port throws an error, then subsequent ports should be tried, with randomly 
increasing wait times in between (a la Ethernet collision avoidance strategy)."
  (declare (ignore port listeners))
  (print-unimplemented-message))
  





