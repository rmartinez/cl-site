(in-package :cl-site-zaserve)

(defparameter *listeners* 5) 

(defun publish-cl-site (&key (output-dir cl-site:*OUTPUT-DIR*))
  (dolist (file (directory (make-pathname :defaults output-dir :name :wild :type "html")))
    (dolist (format-string (list "/~a" "/~a.htm" "/~a.html"))
      (net.aserve:publish-file :path (format nil format-string (pathname-name file))
			       :file (namestring file))))

  (net.aserve:publish-directory :prefix "/"
				:destination  (namestring output-dir)))
  

;;
;; FLAG -- below utility functions borrowed from Gendl Remove when/if
;; Gendl is included in cl-site.  On the other hand, these are a bit
;; of a mess and any feedback is welcome, to clean them up before
;; blindly :use'ing them as-is from Gendl.
;;

(defun client-test (port)
  "Check if a port is busy" ;; FLAG there must be a better way to check for
			    ;; a busy port than this...
  #-ccl
  (multiple-value-bind (result error)
      (ignore-errors  
	(bt:with-timeout (2)
	  (net.aserve.client:do-http-request (format nil "http://127.0.0.1:~a" port))))
    (declare (ignore result))
    (when (typep error 'error)
      port))
  ;;
  ;; CCL has socket problems after calling do-http-request within an
  ;; ignore-errors, when it throws an error. So we do something
  ;; different. But this different thing apparently doesn't work so
  ;; reliably on non-CCL.
  ;;
  #+ccl
  (let* ((result
	  (handler-case
	      (let ((sock (usocket:socket-listen "127.0.0.1" port)))
		(usocket:socket-close sock))
	    (usocket:address-in-use-error (e) (declare (ignore e)) :in-use)
	    (t (e) (declare (ignore e)) :unknown))))
    (unless (or (member result '(:in-use :unknown))
		#+windows-host
		(ignore-errors
		  (net.aserve.client:do-http-request
		      (format nil "http://127.0.0.1:~a" port)))) port)))



(defun start-server (&key (port cl-site:*paserve-port*) ;; FLAG -- change to cl-site:*zaserve-port* when switched to zaserve. 
		       (listeners *listeners*)
		       ;;
		       ;; FLAG -- figure out correct external-format for the other Lisps. 
		       ;;
		       (external-format #+allegro :utf8-base #-allegro :utf8) aserve-start-args)
  (net.aserve:shutdown)
  (let ((wait-time 1))
    (block :outer
      (do () (nil)
	(let ((port port))
	  (block :inner
	    (do ((port-free? (client-test port) (client-test port)))
		(port-free?
		 (format t (if (> wait-time 1) "~&Retrying aserve on ~a...~%"
			       "~&Trying to start aserve on ~a...~%") port)
		 (if (ignore-errors
		       (apply #'net.aserve:start
			      :port port :listeners listeners
			      :external-format external-format
			      aserve-start-args))
		     (return-from :outer port)
		     (progn (sleep (random wait-time)) (return-from :inner))))
	      (incf port))))
	(incf wait-time 0.1)))))


(defun aserve-socket-port (socket)
  (#+(or zacl allegro) socket:local-port
     #+(and (not (or zacl allegro)) acl-compat) acl-compat.socket:local-port socket))


